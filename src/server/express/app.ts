import express, { Request, Response } from "express";
const app = express();

require("custom-env").env();
import cors from "cors";

const PORT = process.env.PORT || 4000;

app.use(cors({
  origin: "*"
})); // cors requests configuration

app.use(express.json()); // to parse JSON bodies

// receives two numbers and returns their sum
app.post("/calculator", (req: Request, res: Response) => {
  const { num1, num2 } = req.body;

  if (!num1) {
    if (Number(num1) != 0) {
      res.status(400).json({
        status: "error",
        message: "num1 is required"
      });
      return;
    }
  }
  if (!num2) {
    if (Number(num2) != 0) {
      res.status(400).json({
        status: "error",
        message: "num2 is required"
      });
      return;
    }
  }

  const sum: number = Number(num1) + Number(num2);

  if (!sum) {
    res.status(400).json({
      status: "error",
      message: "both num1 and num2 is must be a valid numbers"
    });
    return;
  }

  res.status(200).json({
    status: "success",
    message: "numbers added successfully",
    data: {
      sum
    }
  });
});

export {
  app,
  PORT,
}