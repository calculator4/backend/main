import { app, PORT } from "./app";

const server = () => {
  return app.listen(PORT, () => {
    console.log();
    console.log(`Server running on port: ${PORT}.`);
    console.log();
  });
};

export default server;